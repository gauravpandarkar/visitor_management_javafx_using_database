package add_details;

import java.sql.Connection;
import java.sql.Statement;

import db_operations.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import show_options.ShowOption;

public class AddVisitorController {
	static Statement stmt;
	static Connection con;
	@FXML
	private TextField contactNumber;
	@FXML
	private TextField visitorName;
	@FXML
	private TextField date;
	@FXML
	private TextField invitationTime;
	@FXML
	private Button save;
	public void save(ActionEvent event) {
		System.out.println(contactNumber.getText());
		System.out.println(visitorName.getText());
		System.out.println(date.getText());
		System.out.println(invitationTime.getText());
		String query="insert into information(contact_number,visitor_name,check_in_date,check_in_time) values ('" +contactNumber.getText()+ "','" + visitorName.getText()+ "','"+ date.getText()+ "','" +invitationTime.getText()+ "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur addVisitorController "+event.getEventType().getName());
		new ShowOption().show();
	}
	

}
